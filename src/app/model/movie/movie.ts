export class Movie {
  id: number;
  title: string;
  genre: any[];
  year: number;
  duration: number;
  imdbRating: number;
  actors: number[];
  company: number;
  poster: string;
}