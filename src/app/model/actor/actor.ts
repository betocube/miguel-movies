export class Actor {
  id: number;
  first_name: string;
  last_name: any[];
  gender: string;
  bornCity: string;
  birthdate: string;
  movies: number[];
  img: string;
  rating: number;
}