import { Movie } from "../movie/movie";

export class MovieRequestForm {
  movie: Movie;
  isEdit: boolean;
  isDataReady: boolean;
}