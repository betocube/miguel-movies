export class FieldConfig {

  id?: string;
  label?: string;
  name: string;
  placeHolder?: any;
  required?: boolean;
  tooltip: string;

  constructor(options: IFieldConfig) {

    this.id = options.id;
    this.label = options.label;
    this.name = options.name;
    this.placeHolder = options.placeHolder;
    this.required = !!options.required;
    this.tooltip = options.tooltip;
  }
}

export interface IFieldConfig {
  id?: string;
  label?: string;
  name: string;
  placeHolder?: any;
  required?: boolean;
  tooltip?: string;
}
