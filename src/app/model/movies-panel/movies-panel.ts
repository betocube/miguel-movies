import { Company } from './../company/company';
import { Actor } from './../actor/actor';
import { Movie } from './../movie/movie';

export class MoviesPanel {

  movieList: Movie[];
  actorsList: Actor[];
  companiesList: Company[];
  isLoading: boolean;

  constructor() {
    this.movieList = [];
    this.actorsList = [];
    this.companiesList = [];
    this.isLoading = false;
  }
}
