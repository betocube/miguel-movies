
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';

import { RoutePath } from './../../model/route/route.path';
import { AppRoutingNavigationService } from './app-routing-navigation.service';

describe('AppRoutingNavigationService', () => {

  let service: AppRoutingNavigationService;

  let routerMock: RouterMock;

  beforeEach(() => tearUp());

  describe('should navigate to', () => {

    it('MOVIES', () => {

      service.movies();
      routeExpect(`${RoutePath.SIDENAV}/${RoutePath.MOVIES}`);
    });

    it('ACTORS', () => {

      service.actors();
      routeExpect(`${RoutePath.SIDENAV}/${RoutePath.ACTORS}`);
    });

    it('COMPANIES', () => {

      const expectedRoute = `${RoutePath.SIDENAV}/${RoutePath.COMPANIES}`;
      service.companies();
      routeExpect(expectedRoute);
    });
  });

  function tearUp() {

    createMocks();
    configureTestingModule();
    initPropertiesForTest();
  }

  function createMocks() {

    routerMock = new RouterMock();
    spyOn(routerMock, 'navigate').and.stub();
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        AppRoutingNavigationService,
        { provide: Router, useValue: routerMock }
      ]
    });
  }

  function initPropertiesForTest() {
    service = TestBed.inject(AppRoutingNavigationService);
  }

  function routeExpect(routePath: string) {
    expect(routerMock.navigate).toHaveBeenCalledWith([routePath]);
  }
});

class RouterMock {

  navigate(routes: Array<string>) { }
}
