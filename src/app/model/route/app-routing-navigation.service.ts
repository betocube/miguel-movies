import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { RoutePath } from '../../model/route/route.path';
import { Movie } from '../movie/movie';

@Injectable({
  providedIn: 'root'
})
export class AppRoutingNavigationService {

  constructor(private router: Router) { }

  home() {
    this.navigate(RoutePath.SIDENAV);
  }

  movies() {
    this.navigate(`${ RoutePath.SIDENAV }/${ RoutePath.MOVIES }`);
  }

  movieDetail(movie: Movie) {
    const state = { state: { movie } };
    this.navigateWithObject(`${ RoutePath.SIDENAV }/${ RoutePath.MOVIE_DETAIL }/${ movie.id }`, state);
  }

  createMovie() {
    this.navigate(`${ RoutePath.SIDENAV }/${ RoutePath.CREATE_NEW_MOVIE }`);
  }

  editMovie(id: number) {
    this.navigate(`${ RoutePath.SIDENAV }/${ RoutePath.EDIT_MOVIE }/${id}`);
  }

  deleteMovieConfirmation(id: number) {
    this.navigate(`${ RoutePath.SIDENAV }/${ RoutePath.DELETE_MOVIE }/${id}`);
  }

  actors() {
    this.navigate(`${ RoutePath.SIDENAV }/${ RoutePath.ACTORS }`);
  }

  companies() {
    this.navigate(`${ RoutePath.SIDENAV }/${ RoutePath.COMPANIES }`);
  }

  private navigate(routePath: string) {
    this.router.navigate([routePath]);
  }

  private navigateWithObject(routePath: string, data: any) {
    this.router.navigate([routePath], data);
  }
}
