export enum RoutePath {
  MOVIES = 'movies',
  MOVIE_DETAIL = 'movie-detail',
  CREATE_NEW_MOVIE = 'create-new-movie',
  EDIT_MOVIE = 'edit-movie',
  DELETE_MOVIE = 'delete-movie',
  COMPANIES = 'companies',
  ACTORS = 'actors',
  SIDENAV = 'sidenav'
}
