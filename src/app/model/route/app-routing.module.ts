import { SidenavComponent } from './../../component/template/sidenav/component/sidenav.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RoutePath } from './../../model/route/route.path';

export const routes: Routes = [
  {
    path: RoutePath.SIDENAV,
    loadChildren: () => import('../../component/template/sidenav/sidenav.module').then(m => m.SidenavModule)
  },
  {
    path: '',
    redirectTo: '/sidenav',
    pathMatch: 'full'
  },
  { path: '**', component: SidenavComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}
