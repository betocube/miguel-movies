import { Movie } from "../movie/movie";

export class MovieDetails {

  constructor() {
    this.movie = new Movie();
    this.isDataReady = false;
  }
  movie: Movie;
  isDataReady: boolean;
}