import { Component, Input } from '@angular/core';

@Component({
  selector: 'mig-text-2',
  templateUrl: 'text-2.component.html'
})
export class Text2Component {

  @Input() text: string;

  constructor() {}
}
