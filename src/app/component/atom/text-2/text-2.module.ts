import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { Text2Component } from './component/text-2.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    Text2Component
  ],
  exports: [
    Text2Component
  ]
})

export class Text2Module {}
