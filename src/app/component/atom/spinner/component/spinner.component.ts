import { Component, Input } from '@angular/core';

@Component({
  selector: 'mig-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent {

  @Input() size = 72;
  @Input() show: boolean;
  @Input() fixed: boolean;
  @Input() absolute: boolean;
  @Input() background: boolean;
}
