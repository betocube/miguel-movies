import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { Text1Component } from './component/text-1.component';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule
  ],
  declarations: [
    Text1Component
  ],
  exports: [
    Text1Component
  ]
})

export class Text1Module {}
