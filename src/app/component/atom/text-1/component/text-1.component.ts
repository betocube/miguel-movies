import { Component, Input } from '@angular/core';

@Component({
  selector: 'mig-text-1',
  templateUrl: 'text-1.component.html'
})
export class Text1Component {

  @Input() text: string;

  constructor() {}
}
