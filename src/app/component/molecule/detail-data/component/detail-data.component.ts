import { Component, Input } from '@angular/core';

@Component({
  selector: 'mig-detail-data',
  templateUrl: 'detail-data.component.html'
})
export class DetailDataComponent {

  @Input() label: string;
  @Input() labelWidth: string;
  @Input() values: string[];

  constructor() {}
}
