import { Text2Module } from './../../atom/text-2/text-2.module';
import { Text1Module } from './../../atom/text-1/text-1.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';

import { DetailDataComponent } from './component/detail-data.component';

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    Text1Module,
    Text2Module
  ],
  declarations: [
    DetailDataComponent
  ],
  exports: [
    DetailDataComponent
  ]
})

export class DetailDataModule {}
