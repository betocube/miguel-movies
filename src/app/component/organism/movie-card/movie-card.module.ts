import { MovieCardComponent } from './component/movie-card.component';
import { FlexModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { MatCardModule } from '@angular/material/card';

@NgModule({
  declarations: [
    MovieCardComponent
  ],
  imports: [
    CommonModule,
    FlexModule,
    TranslateModule,
    HttpClientModule,
    MatCardModule
  ],
  exports: [
    MovieCardComponent
  ]
})

export class MovieCardModule { }
