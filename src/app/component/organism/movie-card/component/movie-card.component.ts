import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { Component, Input } from '@angular/core';
import { Movie } from '../../../../model/movie/movie';

@Component({
  selector: 'mig-movie-card',
  templateUrl: 'movie-card.component.html',
  styleUrls: ['movie-card.component.scss'],

})
export class MovieCardComponent {

  @Input() movie: Movie;

  constructor(private appRoutingNavigationService: AppRoutingNavigationService) {}

  onCard() {
    this.appRoutingNavigationService.movieDetail(this.movie);
  }
}