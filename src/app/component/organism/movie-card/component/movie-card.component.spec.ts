import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { TranslateModule } from '@ngx-translate/core';
import { MovieCardComponent } from './movie-card.component';
import { MoviesPanel } from './../../../../model/movies-panel/movies-panel';
import { HttpHeaders } from '@angular/common/http';
import { EventEmitter, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Movie } from 'src/app/model/movie/movie';

describe('MovieCardComponent', () => {

  const MAT_CARD = 'mat-card';

  const MOVIES = new MoviesPanel();

  let component: MovieCardComponent;
  let fixture: ComponentFixture<MovieCardComponent>;

  let compiled: HTMLElement;
  let appRoutingNavigationServiceMock: AppRoutingNavigationService;

  beforeEach(() => tearUp());

    describe('and there are movies should', () => {

      beforeEach(() => {
        fixture.detectChanges();
      });

      it('display a mat-car element', () => {
        expect(compiled.querySelector(MAT_CARD)).toBeDefined();
      });
    });

  function tearUp() {

    configureTestingModule();
    initPropertiesForTest();

    fixture.detectChanges();
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ],
      declarations: [
        MovieCardComponent
      ],
      providers: [
        { provide: AppRoutingNavigationService, useValue: appRoutingNavigationServiceMock },
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }

  function initPropertiesForTest() {

    fixture = TestBed.createComponent(MovieCardComponent);
    component = fixture.componentInstance;
    component.movie = new Movie();

    compiled = fixture.nativeElement as HTMLElement;
  }
});
