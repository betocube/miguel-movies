import { FieldConfig } from './../../../../model/fieldConfig/field-config';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MovieDeleteDialogConfigService {

  static MESSAGE_ID = 'TEXT_message';
  static MESSAGE_LABEL = 'mig.movies.deleteConfirmation.message';
  static MESSAGE_NAME = 'message';

  static BUTTON_CANCEL_ID = 'BUTTON_button_cancel';
  static BUTTON_CANCEL_LABEL = 'mig.movies.deleteConfirmation.buttons.cancel';
  static BUTTON_CANCEL_NAME = 'button_cancel';

  static BUTTON_DELETE_ID = 'BUTTON_button_delete';
  static BUTTON_DELETE_LABEL = 'mig.movies.deleteConfirmation.buttons.delete';
  static BUTTON_DELETE_NAME = 'button_delete';

  constructor() {}

  buildMessageConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieDeleteDialogConfigService.MESSAGE_ID,
      label: MovieDeleteDialogConfigService.MESSAGE_LABEL,
      name: MovieDeleteDialogConfigService.MESSAGE_NAME
    });
  }

  buildCancelButtonConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieDeleteDialogConfigService.BUTTON_CANCEL_ID,
      label: MovieDeleteDialogConfigService.BUTTON_CANCEL_LABEL,
      name: MovieDeleteDialogConfigService.BUTTON_CANCEL_NAME,
      required: true
    });
  }

  buildDeleteButtonConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieDeleteDialogConfigService.BUTTON_DELETE_ID,
      label: MovieDeleteDialogConfigService.BUTTON_DELETE_LABEL,
      name: MovieDeleteDialogConfigService.BUTTON_DELETE_NAME,
    });
  }
}
