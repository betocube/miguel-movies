import { Movie } from './../../../../model/movie/movie';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieDeleteDialogService {

  constructor(
    private http: HttpClient
  ) {
  }

  deleteMovie(id: number): Observable<Movie> {
    return this.http.delete<Movie>('http://localhost:3000' + '/movies/' + id);
  }

}
