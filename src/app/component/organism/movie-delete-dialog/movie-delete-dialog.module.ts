import { MovieDeleteDialogRoutingModule } from './movie-delete-dialog-routing.module';
import { DeleteMovieEffect } from './state/effect/movie-delete.effect';
import { MovieDeleteDialogComponent } from './component/movie-delete-dialog.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { DetailDataModule } from './../../molecule/detail-data/detail-data.module';
import { MovieCardModule } from './../movie-card/movie-card.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MovieDeleteDialogComponent
  ],
  entryComponents: [
    MovieDeleteDialogComponent
  ],
  exports: [
    MovieDeleteDialogComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    EffectsModule.forFeature([
      DeleteMovieEffect
    ]),
    MovieCardModule,
    DetailDataModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    FlexLayoutModule,
    MovieDeleteDialogRoutingModule
  ]
})
export class MovieDeleteDialogModule { }
