import { MovieDeleteDialogConfigService } from './../service/movie-delete-dialog-config.service';
import { FieldConfig } from './../../../../model/fieldConfig/field-config';
import { MovieDeleteDialogActionsService } from './../state/service/movie-delete-dialog-actions.service';
import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { ChangeDetectionStrategy, Component, Inject, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'mig-movie-delete-dialog',
  styleUrls: [ './movie-delete-dialog.component.scss' ],
  templateUrl: './movie-delete-dialog.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieDeleteDialogComponent implements OnInit {

  movieId: number;
  isLoading: boolean;

  cancelButtonConfig: FieldConfig;
  deleteButtonConfig: FieldConfig;
  messageConfig: FieldConfig;


  constructor(
    private movieDeleteDialogActionsService: MovieDeleteDialogActionsService,
    private appRoutingNavigationService: AppRoutingNavigationService,
    private movieDeleteDialogConfigService: MovieDeleteDialogConfigService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {}

  ngOnInit() {

    this.initConfigs();
    this.movieId = this.data.movieId;
    this.isLoading = true;
  }

  onCancelDelete() {
    this.appRoutingNavigationService.editMovie(this.movieId);
  }

  onDeleteMovie() {
    this.movieDeleteDialogActionsService.deleteMovie(this.movieId);
  }

  private initConfigs() {
    this.cancelButtonConfig = this.movieDeleteDialogConfigService.buildCancelButtonConfig();
    this.deleteButtonConfig = this.movieDeleteDialogConfigService.buildDeleteButtonConfig();
    this.messageConfig = this.movieDeleteDialogConfigService.buildMessageConfig();
  }
}
