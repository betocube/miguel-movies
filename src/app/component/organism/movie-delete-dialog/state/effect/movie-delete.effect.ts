import { AppRoutingNavigationService } from './../../../../../model/route/app-routing-navigation.service';
import { MovieDeleteActionTypes, MovieDeleteDialogAction, MovieDeleteDialogSuccessAction } from './../action/movie-delete-dialog.action';
import { switchMap, catchError } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { EMPTY, of } from 'rxjs';
import { MovieDeleteDialogService } from '../../service/movie-delete-dialog.service';
import { Movie } from '../../../../../model/movie/movie';

@Injectable()
export class DeleteMovieEffect {

  constructor(
    private actions$: Actions<MovieDeleteDialogAction>,
    private movieDeleteDialogService: MovieDeleteDialogService,
    private appRoutingNavigationService: AppRoutingNavigationService) { }

    deleteMovie$ = createEffect(() => this.actions$
    .pipe(
      ofType<MovieDeleteDialogAction>(MovieDeleteActionTypes.DELETE_MOVIE),
      switchMap((action: MovieDeleteDialogAction) =>  this.movieDeleteDialogService.deleteMovie(action.payload)
      .pipe(
        switchMap(() => {
          this.appRoutingNavigationService.movies();
          return EMPTY;
        }),
        catchError(() => {
          return EMPTY;
        })
      ))
    ),
    { dispatch: false });
}
