import { MovieDeleteDialogAction, MovieDeleteDialogSuccessAction } from './../action/movie-delete-dialog.action';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Movie } from '../../../../../model/movie/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieDeleteDialogActionsService {

  constructor(private store: Store<Movie>) { }

  deleteMovie(id: number): void {
    this.store.dispatch(new MovieDeleteDialogAction(id));
  }

  deleteMovieSuccess(movie: Movie) {
    this.store.dispatch(new MovieDeleteDialogSuccessAction(movie));
  }
}
