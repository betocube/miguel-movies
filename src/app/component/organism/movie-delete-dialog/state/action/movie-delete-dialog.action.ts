import { Movie } from './../../../../../model/movie/movie';
import { Action } from '@ngrx/store';

export enum MovieDeleteActionTypes {
  DELETE_MOVIE = '[DELETE_MOVIE] Delete specific movie',
  DELETE_MOVIE_SUCCESS = '[DELETE_MOVIE] Success',
}

export class MovieDeleteDialogAction implements Action {

  readonly type = MovieDeleteActionTypes.DELETE_MOVIE;

  constructor(public payload: number) {}
}

export class MovieDeleteDialogSuccessAction implements Action {

  readonly type = MovieDeleteActionTypes.DELETE_MOVIE_SUCCESS;

  constructor(public payload: Movie) {}
}

export type MovieDeleteActions = MovieDeleteDialogAction |
MovieDeleteDialogSuccessAction;
