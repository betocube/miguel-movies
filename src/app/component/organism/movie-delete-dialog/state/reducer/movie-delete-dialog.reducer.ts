import { MovieDetails } from './../../../../../model/movie-details/movie-details';
import { MovieDetailsGetActionTypes, MovieDetailsActions } from './../action/movie-details.action';
import { Movie } from './../../../../../model/movie/movie';
import { MoviesPanel } from './../../../../../model/movies-panel/movies-panel';

const defaultState = new MovieDetails();

export function movieDetailsReducer(state: MovieDetails = defaultState, action: MovieDetailsActions): MovieDetails {

  if (action.type === MovieDetailsGetActionTypes.GET_MOVIE_DETAILS_SUCCESS) {
    return decorateMovie(state, action.payload);
  }

  return state;
}

function decorateMovie(state: MovieDetails, movie: Movie): MovieDetails {
  const newState = Object.assign({}, state);

  newState.movie = movie
  newState.isDataReady = true;

  return newState;
}
