import { RoutePath } from './../../../model/route/route.path';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovieDeleteDialogComponent } from './component/movie-delete-dialog.component';

const HEIGHT = '984px';
const WIDTH = '1080px';

const routes: Routes = [
  {
    path: '',
    component: MovieDeleteDialogComponent,
    data: {
      contentComponent: MovieDeleteDialogComponent,
      priority: 3,
      height: HEIGHT,
      maxWidth: WIDTH,
      width: WIDTH
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieDeleteDialogRoutingModule { }