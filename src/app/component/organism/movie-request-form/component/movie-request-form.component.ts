import { MovieRequest } from './../../../../model/movie/movie-request';
import { MovieRequestFormActionsService } from './../state/service/movie-request-form-actions.service';
import { MoviesActionsService } from './../../../template/movies/state/service/movies-actions.service';
import { selectStoreMoviesPanel } from './../../../template/movies/state/selector/movies-panel.selector';
import { FieldConfig } from './../../../../model/fieldConfig/field-config';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { MatChipInputEvent } from '@angular/material/chips';
import { MovieRequestFormConfigService } from '../service/movie-request-form-config.service';
import { MoviesPanel } from '../../../../model/movies-panel/movies-panel';
import { Company } from '../../../../model/company/company';
import { Actor } from '../../../../model/actor/actor';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subscription, Observable } from 'rxjs';
import { Movie } from '../../../../model/movie/movie';
import { FormControl, FormGroup } from '@angular/forms';
import { SidenavActionsService } from '../../../template/sidenav/state/service/sidenav-actions.service';

const PAGE_TITLE_EDIT = 'mig.movies.movieRequest.titleEdit';
const PAGE_TITLE_NEW = 'mig.movies.movieRequest.titleNew';

@Component({
  selector: 'mig-movie-request-form',
  styleUrls: [ './movie-request-form.component.scss' ],
  templateUrl: './movie-request-form.component.html'
})
export class MovieRequestFormComponent implements OnInit, OnDestroy {

  addOnBlur = true;
  readonly separatorKeysCodes = [ENTER, COMMA] as const;
  actorsSelected: string[] = [];

  movieId: number;
  movie: Movie;
  movies: Movie[];
  actors: Actor[] = [];
  actorsInMovie: string[];
  companyInMovie: string[];
  companies: Company[] = [];
  isLoading = false;
  submitIsLoading = false;
  isEditPage = false;

  titleConfig: FieldConfig;
  posterConfig: FieldConfig;
  genreConfig: FieldConfig;
  actorsConfig: FieldConfig;
  companyConfig: FieldConfig;
  yearConfig: FieldConfig;
  durationConfig: FieldConfig;
  scoreConfig: FieldConfig;
  primaryButtonConfig: FieldConfig;

  formGroup = new FormGroup({});

  private moviesPanel$: Observable<MoviesPanel>;
  private moviesPanelSubscription: Subscription;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private movieRequestFormConfigService: MovieRequestFormConfigService,
    private moviesActionsService: MoviesActionsService,
    private movieRequestFormActionsService: MovieRequestFormActionsService,
    private router: ActivatedRoute,
    private store: Store<Movie>,
    private sidenavActionsService: SidenavActionsService
  ) {}

  ngOnInit() {

    this.router.paramMap.subscribe( paramMap => {

      this.movieId = +paramMap.get('id');
      if (this.movieId) {
        this.isEditPage = true;
      }
      const pageTitle = this.isEditPage ? PAGE_TITLE_EDIT : PAGE_TITLE_NEW;
      this.sidenavActionsService.setTitle(pageTitle);
    });

    this.initFormGroup();
    this.initConfigs();
    this.initSubscriptions();
    this.initActors();
    this.initCompanies();
  }

  ngOnDestroy() {

    this.moviesPanelSubscription.unsubscribe();
  }

  private initSubscriptions() {

    this.moviesPanel$ = this.store.pipe(select(selectStoreMoviesPanel));
    this.moviesPanelSubscription = this.moviesPanel$.subscribe(moviesPanel => {
      this.isLoading = false;
      this.movies = moviesPanel?.movieList;
      this.movie = this.movies?.find((movie) => movie.id === this.movieId);

      this.actors = moviesPanel?.actorsList;
      this.companies = moviesPanel?.companiesList;
      if (this.isEditPage && this.movie && this.actors) {

        this.movieRequestFormConfigService.setMovieData(this.formGroup, this.movie);
        this.movieRequestFormConfigService.setActorsData(this.formGroup, this.actors, this.movie?.actors);
        this.movieRequestFormConfigService.setCompanyData(this.formGroup, this.companies, this.movie?.company);
      }
      this.changeDetector.detectChanges();
    });
  }

  initActors() {
    this.isLoading = true;
    this.moviesActionsService.getActorsList();
  }

  initCompanies() {
    this.isLoading = true;
    this.moviesActionsService.getCompaniesList();
  }

  private initFormGroup() {
    this.formGroup = this.movieRequestFormConfigService.createFormGroup();
  }

  private initConfigs() {

    this.titleConfig = this.movieRequestFormConfigService.buildTitleInputConfig();
    this.posterConfig = this.movieRequestFormConfigService.buildPosterInputConfig();
    this.genreConfig = this.movieRequestFormConfigService.buildGenreInputConfig();
    this.actorsConfig = this.movieRequestFormConfigService.buildActorsInputConfig();
    this.companyConfig = this.movieRequestFormConfigService.buildCompanyInputConfig();
    this.yearConfig = this.movieRequestFormConfigService.buildYearInputConfig();
    this.durationConfig = this.movieRequestFormConfigService.buildDurationInputConfig();
    this.scoreConfig = this.movieRequestFormConfigService.buildScoreInputConfig();
    this.primaryButtonConfig = this.movieRequestFormConfigService.buildPrimaryButtonConfig(this.isEditPage);
  }

  addGenre(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();
    if (value) {
      this.getFormControl(this.genreConfig.name).setValue([...this.getFormControl(this.genreConfig.name).value, value]);
      this.getFormControl(this.genreConfig.name).updateValueAndValidity();
    }
    event.chipInput!.clear();
  }

  removeGenre(genre: string): void {

    const newValue = this.getFormControl(this.genreConfig.name).value.filter(e => e !== genre);
    this.getFormControl(this.genreConfig.name).setValue(newValue);
  }

  onSubmit() {

    this.submitIsLoading = true;
    this.isLoading = true;

    const request: MovieRequest = this.doRequest();

    this.isEditPage ? this.movieRequestFormActionsService.editMovie(request, this.movieId) : this.movieRequestFormActionsService.createNewMovie(request);

  }

  getFormControl(controlName: string): FormControl {
    return this.formGroup.get(controlName) as FormControl;
  }

  isFormValid(): boolean {
    return this.formGroup.valid;
  }

  private doRequest() {

    const request: MovieRequest = new MovieRequest();

    request.actors = this.formGroup.value['actors'].map((actor) => actor.id);
    request.company = this.formGroup.value['company'] ? this.formGroup.value['company']['id'] : undefined;
    request.genre = this.formGroup.value['genre'];
    request.duration = this.formGroup.value['duration'];
    request.poster = this.formGroup.value['poster'];
    request.imdbRating = this.formGroup.value['imdbRating'];
    request.title = this.formGroup.value['title'];
    request.year = this.formGroup.value['year'];

    return request;
  }
}
