import { MovieRequestForm } from './../../../../../model/movie-request-form/movie-request-form';
import { createFeatureSelector } from '@ngrx/store';

export const selectStoreMovieDetails = createFeatureSelector<MovieRequestForm>('movieRequestForm');
