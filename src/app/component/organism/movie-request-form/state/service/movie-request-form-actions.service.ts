import { CreateNewMovieAction, CreateNewMovieActionSuccess, EditMovieAction } from './../action/movie-request-form.action';
import { MovieRequest } from './../../../../../model/movie/movie-request';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Movie } from '../../../../../model/movie/movie';

@Injectable({
  providedIn: 'root'
})
export class MovieRequestFormActionsService {

  constructor(private store: Store<Movie>) { }

  createNewMovie(movieRequest: MovieRequest): void {
    this.store.dispatch(new CreateNewMovieAction(movieRequest));
  }

  createNewMovieSuccess(movie: Movie) {
    this.store.dispatch(new CreateNewMovieActionSuccess(movie));
  }

  editMovie(movieRequest: MovieRequest, movieId: number) {
    this.store.dispatch(new EditMovieAction(movieRequest, movieId));
  }
}
