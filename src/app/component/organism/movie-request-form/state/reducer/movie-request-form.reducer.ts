import { MovieRequestForm } from './../../../../../model/movie-request-form/movie-request-form';
import { MovieRequestFormActionTypes, MovieRequestFormActions } from './../action/movie-request-form.action';
import { Movie } from './../../../../../model/movie/movie';

const defaultState = new MovieRequestForm();

export function movieRequestFormReducer(state: MovieRequestForm = defaultState, action: MovieRequestFormActions): MovieRequestForm {

  switch (action.type) {

    case MovieRequestFormActionTypes.CREATE_NEW_MOVIE_SUCCESS:
      return decorateMovie(state, action.payload);

    case MovieRequestFormActionTypes.EDIT_MOVIE_SUCCESS:
      return decorateMovie(state, action.payload);

    default:
      return state;
  }
}

function decorateMovie(state: MovieRequestForm, movie: Movie): MovieRequestForm {
  const newState = Object.assign({}, state);

  newState.movie = movie
  newState.isDataReady = true;

  return newState;
}
