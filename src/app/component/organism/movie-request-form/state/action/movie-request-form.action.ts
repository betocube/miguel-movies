import { MovieRequest } from './../../../../../model/movie/movie-request';
import { Movie } from './../../../../../model/movie/movie';
import { Action } from '@ngrx/store';

export enum MovieRequestFormActionTypes {
  CREATE_NEW_MOVIE = '[MOVIE_REQUEST_FORM] Create new movie',
  CREATE_NEW_MOVIE_SUCCESS = '[MOVIE_REQUEST_FORM] Create new movie Success',
  EDIT_MOVIE = '[MOVIE_REQUEST_FORM] Edit movie',
  EDIT_MOVIE_SUCCESS = '[MOVIE_REQUEST_FORM] Edit movie Success',
}

export class CreateNewMovieAction implements Action {

  readonly type = MovieRequestFormActionTypes.CREATE_NEW_MOVIE;

  constructor(public payload: MovieRequest) {}
}

export class CreateNewMovieActionSuccess implements Action {

  readonly type = MovieRequestFormActionTypes.CREATE_NEW_MOVIE_SUCCESS;

  constructor(public payload: Movie) {}
}

export class EditMovieAction implements Action {

  readonly type = MovieRequestFormActionTypes.EDIT_MOVIE;

  constructor(public payload: MovieRequest, public movieId: number) {}
}

export class EditMovieActionSuccess implements Action {

  readonly type = MovieRequestFormActionTypes.EDIT_MOVIE_SUCCESS;

  constructor(public payload: Movie) {}
}

export type MovieRequestFormActions = CreateNewMovieActionSuccess |
CreateNewMovieAction |
EditMovieAction |
EditMovieActionSuccess;
