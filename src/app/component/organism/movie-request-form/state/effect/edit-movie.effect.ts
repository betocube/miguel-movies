import { AppRoutingNavigationService } from './../../../../../model/route/app-routing-navigation.service';
import { MovieRequestFormService } from './../../service/movie-request-form.service';
import { MovieRequestFormActionTypes, EditMovieAction, EditMovieActionSuccess } from './../action/movie-request-form.action';
import { catchError, switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { EMPTY, of } from 'rxjs';
import { Movie } from '../../../../../model/movie/movie';

@Injectable()
export class EditMovieEffect {

  constructor(
    private actions$: Actions<EditMovieAction>,
    private movieRequestFormService: MovieRequestFormService,
    private appRoutingNavigationService: AppRoutingNavigationService) { }

    createMoviel$ = createEffect(() => this.actions$
    .pipe(
      ofType<EditMovieAction>(MovieRequestFormActionTypes.EDIT_MOVIE),
      switchMap((action: EditMovieAction) =>  this.movieRequestFormService.putMovie(action.payload, action.movieId)
      .pipe(
          switchMap((movie: Movie) => {
            this.appRoutingNavigationService.movieDetail(movie);
            return EMPTY;
          }),
          catchError(() => {
            return EMPTY;
          })
      ))
    ),
    { dispatch: false });
}
