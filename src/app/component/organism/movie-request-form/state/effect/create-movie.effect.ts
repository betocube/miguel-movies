import { MovieRequestFormService } from './../../service/movie-request-form.service';
import { CreateNewMovieAction, MovieRequestFormActionTypes, CreateNewMovieActionSuccess } from './../action/movie-request-form.action';
import { switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { Movie } from '../../../../../model/movie/movie';

@Injectable()
export class CreateMovieEffect {

  constructor(
    private actions$: Actions<CreateNewMovieAction>,
    private movieRequestFormService: MovieRequestFormService) { }

    createMoviel$ = createEffect(() => this.actions$
    .pipe(
      ofType<CreateNewMovieAction>(MovieRequestFormActionTypes.CREATE_NEW_MOVIE),
      switchMap((action: CreateNewMovieAction) =>  this.movieRequestFormService.postNewMovie(action.payload).pipe(
          switchMap((movie: Movie) => of(new CreateNewMovieActionSuccess(movie)))
      ))
    ));
}
