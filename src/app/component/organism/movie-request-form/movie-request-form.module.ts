import { SpinnerModule } from './../../atom/spinner/spinner.module';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { CreateMovieEffect } from './state/effect/create-movie.effect';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MovieRequestFormRoutingModule } from './movie-request-form-routing.module';
import { DetailDataModule } from '../../molecule/detail-data/detail-data.module';
import { MovieCardModule } from '../movie-card/movie-card.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';

import { MovieRequestFormComponent } from './component/movie-request-form.component';
import { movieRequestFormReducer } from './state/reducer/movie-request-form.reducer';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatChipsModule } from '@angular/material/chips';
import { EditMovieEffect } from './state/effect/edit-movie.effect';

@NgModule({
  declarations: [
    MovieRequestFormComponent
  ],
  entryComponents: [
    MovieRequestFormComponent
  ],
  exports: [
    MovieRequestFormComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    MovieCardModule,
    FormsModule,
    DetailDataModule,
    EffectsModule.forFeature([
      CreateMovieEffect,
      EditMovieEffect
    ]),
    MovieRequestFormRoutingModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatInputModule,
    MatChipsModule,
    MatIconModule,
    MatButtonModule,
    SpinnerModule,
    StoreModule.forFeature('movieRequestForm', movieRequestFormReducer),
  ]
})
export class MovieRequestModule { }
