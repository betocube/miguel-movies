import { MovieRequestFormComponent } from './component/movie-request-form.component';
import { RoutePath } from '../../../model/route/route.path';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    component: MovieRequestFormComponent,
    data: {
      contentComponent: MovieRequestFormComponent,
      onCloseNavigateToThisRoute: `${ RoutePath.SIDENAV }/${ RoutePath.MOVIES }`
    }
  },
  {
    path: '/:id',
    component: MovieRequestFormComponent,
    data: {
      contentComponent: MovieRequestFormComponent,
      onCloseNavigateToThisRoute: `${ RoutePath.SIDENAV }/${ RoutePath.MOVIE_DETAIL }/:id`
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieRequestFormRoutingModule { }