import { MovieRequest } from './../../../../model/movie/movie-request';
import { Movie } from './../../../../model/movie/movie';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieRequestFormService {

  constructor(
    private http: HttpClient
  ) {
  }

  postNewMovie(movieRequest: MovieRequest): Observable<Movie> {
    return this.http.post<Movie>('http://localhost:3000/movies', movieRequest);
  }

  putMovie(movieRequest: MovieRequest, movieId: number): Observable<Movie> {
    return this.http.put<Movie>('http://localhost:3000/movies/'+movieId, movieRequest);
  }

}
