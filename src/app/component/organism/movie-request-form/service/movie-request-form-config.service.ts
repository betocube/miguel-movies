import { Company } from './../../../../model/company/company';
import { Actor } from './../../../../model/actor/actor';
import { Movie } from '../../../../model/movie/movie';
import { MovieRequest } from './../../../../model/movie/movie-request';
import { FieldConfig } from '../../../../model/fieldConfig/field-config';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class MovieRequestFormConfigService {

  static TITLE_ID = 'INPUT_title';
  static TITLE_LABEL = 'mig.movies.movieRequest.labels.title';
  static TITLE_NAME = 'title';
  static TITLE_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.title';

  static COMPANY_ID = 'INPUT_company';
  static COMPANY_LABEL = 'mig.movies.movieRequest.labels.company';
  static COMPANY_NAME = 'company';
  static COMPANY_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.company';

  static POSTER_ID = 'INPUT_poster';
  static POSTER_LABEL = 'mig.movies.movieRequest.labels.poster';
  static POSTER_NAME = 'poster';
  static POSTER_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.poster';

  static GENRE_ID = 'INPUT_genre';
  static GENRE_LABEL = 'mig.movies.movieRequest.labels.genre';
  static GENRE_NAME = 'genre';
  static GENRE_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.genre';

  static ACTORS_ID = 'INPUT_actors';
  static ACTORS_LABEL = 'mig.movies.movieRequest.labels.actors';
  static ACTORS_NAME = 'actors';
  static ACTORS_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.actors';

  static YEAR_ID = 'INPUT_year';
  static YEAR_LABEL = 'mig.movies.movieRequest.labels.year';
  static YEAR_NAME = 'year';
  static YEAR_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.year';

  static DURATION_ID = 'INPUT_duration';
  static DURATION_LABEL = 'mig.movies.movieRequest.labels.duration';
  static DURATION_NAME = 'duration';
  static DURATION_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.duration';

  static SCORE_ID = 'INPUT_score';
  static SCORE_LABEL = 'mig.movies.movieRequest.labels.score';
  static SCORE_NAME = 'imdbRating';
  static SCORE_PLACEHOLDER = 'mig.movies.movieRequest.placeholder.score';

  static BUTTON_ID = 'BUTTON_button';
  static BUTTON_LABEL_CREATE = 'mig.movies.movieRequest.labels.buttonCreate';
  static BUTTON_LABEL_EDIT = 'mig.movies.movieRequest.labels.buttonEdit';
  static BUTTON_NAME = 'button';

  constructor() {}

  createFormGroup(): FormGroup {

    return new FormGroup({
      [MovieRequestFormConfigService.TITLE_NAME]: new FormControl('', Validators.required),
      [MovieRequestFormConfigService.POSTER_NAME]: new FormControl('', Validators.required),
      [MovieRequestFormConfigService.COMPANY_NAME]: new FormControl([]),
      [MovieRequestFormConfigService.ACTORS_NAME]: new FormControl([], Validators.required),
      [MovieRequestFormConfigService.GENRE_NAME]: new FormControl([], Validators.required),
      [MovieRequestFormConfigService.YEAR_NAME]: new FormControl('', [Validators.required, Validators.min(1800), Validators.max(2022)]),
      [MovieRequestFormConfigService.DURATION_NAME]: new FormControl('', [Validators.required, Validators.min(0.0)]),
      [MovieRequestFormConfigService.SCORE_NAME]: new FormControl('', [Validators.required, Validators.min(0.0), Validators.max(10)]),
    });
  }

  buildTitleInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.TITLE_ID,
      label: MovieRequestFormConfigService.TITLE_LABEL,
      name: MovieRequestFormConfigService.TITLE_NAME,
      placeHolder: MovieRequestFormConfigService.TITLE_PLACEHOLDER,
      required: true
    });
  }

  buildPosterInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.POSTER_ID,
      label: MovieRequestFormConfigService.POSTER_LABEL,
      name: MovieRequestFormConfigService.POSTER_NAME,
      placeHolder: MovieRequestFormConfigService.POSTER_PLACEHOLDER,
      required: true
    });
  }

  buildCompanyInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.COMPANY_ID,
      label: MovieRequestFormConfigService.COMPANY_LABEL,
      name: MovieRequestFormConfigService.COMPANY_NAME,
      placeHolder: MovieRequestFormConfigService.COMPANY_PLACEHOLDER,
      required: true
    });
  }

  buildActorsInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.ACTORS_ID,
      label: MovieRequestFormConfigService.ACTORS_LABEL,
      name: MovieRequestFormConfigService.ACTORS_NAME,
      placeHolder: MovieRequestFormConfigService.ACTORS_PLACEHOLDER,
      required: true
    });
  }

  buildGenreInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.GENRE_ID,
      label: MovieRequestFormConfigService.GENRE_LABEL,
      name: MovieRequestFormConfigService.GENRE_NAME,
      placeHolder: MovieRequestFormConfigService.GENRE_PLACEHOLDER,
      required: true
    });
  }

  buildYearInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.YEAR_ID,
      label: MovieRequestFormConfigService.YEAR_LABEL,
      name: MovieRequestFormConfigService.YEAR_NAME,
      placeHolder: MovieRequestFormConfigService.YEAR_PLACEHOLDER,
      required: true
    });
  }

  buildDurationInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.DURATION_ID,
      label: MovieRequestFormConfigService.DURATION_LABEL,
      name: MovieRequestFormConfigService.DURATION_NAME,
      placeHolder: MovieRequestFormConfigService.DURATION_PLACEHOLDER,
      required: true
    });
  }

  buildScoreInputConfig(): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.SCORE_ID,
      label: MovieRequestFormConfigService.SCORE_LABEL,
      name: MovieRequestFormConfigService.SCORE_NAME,
      placeHolder: MovieRequestFormConfigService.SCORE_PLACEHOLDER,
      required: true
    });
  }

  buildPrimaryButtonConfig(isEdit: boolean): FieldConfig {
    return new FieldConfig({
      id: MovieRequestFormConfigService.BUTTON_ID,
      label: isEdit ? MovieRequestFormConfigService.BUTTON_LABEL_EDIT : MovieRequestFormConfigService.BUTTON_LABEL_CREATE,
      name: MovieRequestFormConfigService.BUTTON_NAME,
    });
  }

  setMovieData(formGroup: FormGroup, movieInfo: Movie) {

      formGroup.get(MovieRequestFormConfigService.TITLE_NAME)?.setValue(movieInfo.title);
      formGroup.get(MovieRequestFormConfigService.POSTER_NAME)?.setValue(movieInfo.poster);
      formGroup.get(MovieRequestFormConfigService.GENRE_NAME)?.setValue(movieInfo.genre);
      formGroup.get(MovieRequestFormConfigService.YEAR_NAME)?.setValue(movieInfo.year);
      formGroup.get(MovieRequestFormConfigService.DURATION_NAME)?.setValue(movieInfo.duration);
      formGroup.get(MovieRequestFormConfigService.SCORE_NAME)?.setValue(movieInfo.imdbRating);
  }

  setActorsData(formGroup: FormGroup, allActors: Actor[], actorsSelected: number[]) {

    const actorsToSet = allActors?.filter((actor) => {
      return actorsSelected?.find((actorId) => {
        return actor.id === actorId;
      });
    });
    formGroup.get(MovieRequestFormConfigService.ACTORS_NAME)?.setValue(actorsToSet);
}

  setCompanyData(formGroup: FormGroup, allCompanies: Company[], companySelected: number) {

    const companyToSet = allCompanies?.find((company) => {
      return companySelected === company.id;
    });

    formGroup.get(MovieRequestFormConfigService.COMPANY_NAME)?.setValue(companyToSet);
  }

  private getFormControl(formGroup: FormGroup, formControlName: string): FormControl {
    return formGroup.get(formControlName) as FormControl;
  }
}
