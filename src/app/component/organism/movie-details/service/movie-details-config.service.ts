import { of, Observable } from 'rxjs';
import { Company } from './../../../../model/company/company';
import { Actor } from './../../../../model/actor/actor';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsConfigService {

  buildActorsString(actors: Actor[], actorsId: number[]): Observable<string[]> {

    return of(actors
    .filter((actor, index) => {
      return actorsId.find((id) => actor.id === id);
    })
    .map((actor) => `${actor.first_name} ${actor.last_name}`));
  }


  buildCompanyString(companies: Company[], companyId: number): Observable<string> {

    return of(companies
    .find((company) => {
      return companyId === company.id;
    })?.name);
  }
}
