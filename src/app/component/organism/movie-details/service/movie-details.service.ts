import { Movie } from './../../../../model/movie/movie';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsService {

  constructor(
    private http: HttpClient
  ) {
  }

  getMovieDetails(id: number): Observable<Movie> {
    return this.http.get<Movie>('http://localhost:3000' + '/movies/' + id);
  }

}
