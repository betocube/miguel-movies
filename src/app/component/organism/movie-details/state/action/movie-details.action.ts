import { Movie } from './../../../../../model/movie/movie';
import { Action } from '@ngrx/store';

export enum MovieDetailsGetActionTypes {
  GET_MOVIE_DETAILS = '[MOVIE_DETAILS] Get specific movieDetails',
  GET_MOVIE_DETAILS_SUCCESS = '[MOVIE_DETAILS] Success',
  RESET_MOVIE_DETAILS = '[MOVIE_DETAILS] Reset',
}

export class MovieDetailsGetAction implements Action {

  readonly type = MovieDetailsGetActionTypes.GET_MOVIE_DETAILS;

  constructor(public payload: number) {}
}

export class MovieDetailsGetSuccessAction implements Action {

  readonly type = MovieDetailsGetActionTypes.GET_MOVIE_DETAILS_SUCCESS;

  constructor(public payload: Movie) {}
}

export class ResetMovieDetailsAction implements Action {

  readonly type = MovieDetailsGetActionTypes.RESET_MOVIE_DETAILS;
}

export type MovieDetailsActions = MovieDetailsGetSuccessAction |
MovieDetailsGetAction |
ResetMovieDetailsAction;
