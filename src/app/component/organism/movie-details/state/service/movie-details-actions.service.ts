import { ResetMovieDetailsAction } from './../action/movie-details.action';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Movie } from '../../../../../model/movie/movie';
import { MovieDetailsGetAction, MovieDetailsGetSuccessAction } from '../action/movie-details.action';

@Injectable({
  providedIn: 'root'
})
export class MovieDetailsActionsService {

  constructor(private store: Store<Movie>) { }

  getMovieDetails(id: number): void {
    this.store.dispatch(new MovieDetailsGetAction(id));
  }

  getMovieDetailsSuccess(movie: Movie) {
    this.store.dispatch(new MovieDetailsGetSuccessAction(movie));
  }

  resetMovieDetails() {
    this.store.dispatch(new ResetMovieDetailsAction());
  }
}
