import { MovieDetails } from './../../../../../model/movie-details/movie-details';
import { createFeatureSelector } from '@ngrx/store';

export const selectStoreMovieDetails = createFeatureSelector<MovieDetails>('movieDetails');
