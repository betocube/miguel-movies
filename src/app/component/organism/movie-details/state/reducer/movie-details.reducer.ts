import { MovieDetails } from './../../../../../model/movie-details/movie-details';
import { MovieDetailsGetActionTypes, MovieDetailsActions } from './../action/movie-details.action';
import { Movie } from './../../../../../model/movie/movie';

const defaultState = new MovieDetails();

export function movieDetailsReducer(state: MovieDetails = defaultState, action: MovieDetailsActions): MovieDetails {

  switch (action.type) {
    case MovieDetailsGetActionTypes.GET_MOVIE_DETAILS_SUCCESS:
      return decorateMovie(state, action.payload);

    case MovieDetailsGetActionTypes.RESET_MOVIE_DETAILS:
      return new MovieDetails();

    default:
      return state;
  }
}

function decorateMovie(state: MovieDetails, movie: Movie): MovieDetails {
  const newState = Object.assign({}, state);

  newState.movie = movie
  newState.isDataReady = true;

  return newState;
}
