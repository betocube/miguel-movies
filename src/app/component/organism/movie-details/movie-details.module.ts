import { SpinnerModule } from './../../atom/spinner/spinner.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { DetailDataModule } from './../../molecule/detail-data/detail-data.module';
import { MovieCardModule } from './../movie-card/movie-card.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';

import { MovieDetailsComponent } from './component/movie-details.component';
import { GetMovieDetailsEffect } from './state/effect/get-movie-details.effect';
import { movieDetailsReducer } from './state/reducer/movie-details.reducer';
import { MovieDetailsRoutingModule } from './movie-details-routing.module';
import { MovieDeleteDialogModule } from '../movie-delete-dialog/movie-delete-dialog.module';

@NgModule({
  declarations: [
    MovieDetailsComponent
  ],
  entryComponents: [
    MovieDetailsComponent
  ],
  exports: [
    MovieDetailsComponent
  ],
  imports: [
    TranslateModule,
    CommonModule,
    EffectsModule.forFeature([
      GetMovieDetailsEffect
    ]),
    MovieCardModule,
    DetailDataModule,
    MatIconModule,
    MatButtonModule,
    MovieDetailsRoutingModule,
    MovieDeleteDialogModule,
    FlexLayoutModule,
    SpinnerModule,
    StoreModule.forFeature('movieDetails', movieDetailsReducer),
  ]
})
export class MovieDetailsModule { }
