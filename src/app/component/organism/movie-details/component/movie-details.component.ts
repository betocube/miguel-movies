import { MovieDetails } from './../../../../model/movie-details/movie-details';
import { Movie } from './../../../../model/movie/movie';
import { MovieDeleteDialogComponent } from './../../movie-delete-dialog/component/movie-delete-dialog.component';
import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { MovieDetailsConfigService } from './../service/movie-details-config.service';
import { MoviesActionsService } from './../../../template/movies/state/service/movies-actions.service';
import { selectStoreMoviesPanel } from './../../../template/movies/state/selector/movies-panel.selector';
import { MoviesPanel } from './../../../../model/movies-panel/movies-panel';
import { Company } from './../../../../model/company/company';
import { Actor } from './../../../../model/actor/actor';
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Navigation, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';
import { Subscription, Observable, Subject, forkJoin } from 'rxjs';

import { selectStoreMovieDetails } from '../state/selector/movie-details.selector';
import { MovieDetailsActionsService } from '../state/service/movie-details-actions.service';
import { MatDialog } from '@angular/material/dialog';
import { takeUntil, switchMap } from 'rxjs/operators';
import { SidenavActionsService } from '../../../template/sidenav/state/service/sidenav-actions.service';

@Component({
  selector: 'mig-movie-details',
  styleUrls: [ './movie-details.component.scss' ],
  templateUrl: './movie-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MovieDetailsComponent implements OnInit, OnDestroy {

  movie: Movie = new Movie();
  movies: Movie[] = [];
  actors: Actor[] = [];
  actorsInMovie: string[];
  companyInMovie: string;
  companies: Company[] = [];
  isLoading: boolean;
  nav: Navigation;

  private moviesPanel$: Observable<MoviesPanel>;
  private moviesPanelSubscription: Subscription;
  private onDestroyer$: Subject<void>;

  private subscription: Subscription;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private movieDetailsActionsService: MovieDetailsActionsService,
    private moviesActionsService: MoviesActionsService,
    private movieDetailsConfigService: MovieDetailsConfigService,
    private appRoutingNavigationService: AppRoutingNavigationService,
    private router: Router,
    private store: Store<Movie>,
    private dialog: MatDialog,
    private sidenavActionsService: SidenavActionsService
  ) {
    this.onDestroyer$ = new Subject<void>();
    this.nav = this.router.getCurrentNavigation();
  }

  ngOnInit() {

    this.isLoading = true;

    this.initSubscriptions();
    this.initActors();
    this.initCompanies();
  }

  ngOnDestroy() {

    this.movieDetailsActionsService.resetMovieDetails();
    this.onDestroyer$.next();
    this.onDestroyer$.complete();
    this.moviesPanelSubscription.unsubscribe();
  }

  onEditMovie() {
    this.appRoutingNavigationService.editMovie(this.movie.id);
  }

  onDeleteMovie() {
    const dialogRef = this.dialog.open(MovieDeleteDialogComponent, { data: {movieId: this.movie.id}});
  }

  private initActors() {
    this.moviesActionsService.getActorsList();
  }

  private initCompanies() {
    this.moviesActionsService.getCompaniesList();
  }

  private initSubscriptions() {

    this.initRouteSubscription();
    this.initmoviesPanelSubscription();
  }

  private initRouteSubscription() {

    if (this.nav?.extras && this.nav?.extras?.state && this.nav?.extras?.state?.movie) {
      this.movie = this.nav.extras.state.movie as Movie;
    }
  }

  private initmoviesPanelSubscription() {

    this.moviesPanel$ = this.store.pipe(select(selectStoreMoviesPanel));
    this.moviesPanelSubscription = this.moviesPanel$.subscribe(moviesPanel => {
      this.actors = moviesPanel?.actorsList.length > 0 ? moviesPanel?.actorsList : undefined;
      this.companies = moviesPanel?.companiesList;

      if (!this.actors) {
        this.appRoutingNavigationService.movies();
      }

      if (this.actors && this.companies) {
        forkJoin({
          actorsInMovie: this.movieDetailsConfigService.buildActorsString(this.actors, this.movie.actors),
          companyInMovie: this.movieDetailsConfigService.buildCompanyString(this.companies, this.movie.company)
        }).subscribe((result) => {
          this.actorsInMovie = result.actorsInMovie;
          this.companyInMovie = result.companyInMovie;
          this.isLoading = false;
          this.changeDetector.detectChanges();
        });
      }


    });
  }
}
