import { RoutePath } from './../../../model/route/route.path';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MovieDetailsComponent } from './component/movie-details.component';


const routes: Routes = [
  {
    path: '',
    component: MovieDetailsComponent,
    data: {
      contentComponent: MovieDetailsComponent,
      onCloseNavigateToThisRoute: `${ RoutePath.SIDENAV }/${ RoutePath.MOVIES }`
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieDetailsRoutingModule { }