import { SpinnerModule } from './../../atom/spinner/spinner.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { GetCompaniesListEffect } from './state/effect/get-companies-list.effect';
import { GetActorsListEffect } from './state/effect/get-actors-list.effect';
import { StoreModule } from '@ngrx/store';
import { GetMoviesListEffect } from './state/effect/get-movies-list.effect';
import { MatCardModule } from '@angular/material/card';
import { MovieCardModule } from '../../organism/movie-card/movie-card.module';
import { MoviesRoutingModule } from './movies-routing.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { MoviesComponent } from './component/movies.component';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { EffectsModule } from '@ngrx/effects';
import { moviesPanelReducer } from './state/reducer/movies-panel.reducer';

@NgModule({
  declarations: [
    MoviesComponent
  ],
  imports: [
    CommonModule,
    FlexLayoutModule,
    TranslateModule,
    HttpClientModule,
    EffectsModule.forFeature([
      GetMoviesListEffect,
      GetActorsListEffect,
      GetCompaniesListEffect
    ]),
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MovieCardModule,
    SpinnerModule,
    MoviesRoutingModule,
    StoreModule.forFeature('moviesPanel', moviesPanelReducer)
  ]
})

export class MoviesModule { }
