import { SidenavActionsService } from './../../sidenav/state/service/sidenav-actions.service';
import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { Company } from './../../../../model/company/company';
import { Actor } from './../../../../model/actor/actor';
import { selectStoreMoviesPanel } from './../state/selector/movies-panel.selector';
import { Store, select } from '@ngrx/store';
import { MoviesPanel } from './../../../../model/movies-panel/movies-panel';
import { MoviesActionsService } from '../state/service/movies-actions.service';
import { Movie } from './../../../../model/movie/movie';
import { Observable, Subscription } from 'rxjs';
import { Component, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'mig-movies',
  styleUrls: ['movies.component.scss'],
  templateUrl: 'movies.component.html'
})
export class MoviesComponent implements OnInit, OnDestroy {

  pageTitle: string = 'mig.movies.title';

  movies: Movie[] = [];
  actors: Actor[] = [];
  companies: Company[] = [];
  isLoading: boolean;
  private moviesPanel$: Observable<MoviesPanel>;
  private moviesPanelSubscription: Subscription;

  constructor(
    private moviesActionsService: MoviesActionsService,
    private store: Store<MoviesPanel>,
    private appRoutingNavigationService: AppRoutingNavigationService,
    private sidenavActionsService: SidenavActionsService) {}

  ngOnInit() {

    this.sidenavActionsService.setTitle(this.pageTitle);
    this.initList();
    this.initActors();
    this.initCompanies();
    this.initSubscription();
  }

  ngOnDestroy() {

    this.moviesPanelSubscription.unsubscribe();
  }

  initList() {
    this.moviesActionsService.getMoviesList();
  }

  goToAddMovie() {
    this.appRoutingNavigationService.createMovie();
  }

  private initSubscription() {

    this.moviesPanel$ = this.store.pipe(select(selectStoreMoviesPanel));
    this.moviesPanelSubscription = this.moviesPanel$.subscribe(moviesPanel => {
      this.isLoading = moviesPanel?.isLoading;
      this.movies = moviesPanel?.movieList;
      this.actors = moviesPanel?.actorsList;
      this.companies = moviesPanel?.companiesList;
    });
  }

  private initActors() {
    this.moviesActionsService.getActorsList();
  }

  private initCompanies() {
    this.moviesActionsService.getCompaniesList();
  }
}