import { SidenavActionsService } from './../../sidenav/state/service/sidenav-actions.service';
import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { MoviesActionsService } from './../state/service/movies-actions.service';
import { MoviesComponent } from './movies.component';
import { MoviesPanel } from './../../../../model/movies-panel/movies-panel';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { of, ReplaySubject } from 'rxjs';
import { Movie } from 'src/app/model/movie/movie';

describe('MoviesComponent', () => {

  const MOVIE_GRID = 'movie-grid';
  const MOVIES = new MoviesPanel();

  let component: MoviesComponent;
  let fixture: ComponentFixture<MoviesComponent>;

  let sidenavActionsServiceMock: SidenavActionsService;
  let appRoutingNavigationServiceMock: AppRoutingNavigationService;
  let moviesActionsServiceMock: MoviesActionsService;
  let storeMock: Store<MoviesPanel>;
  let storeSubject$: ReplaySubject<MoviesPanel>;

  let compiled: HTMLElement;

  beforeEach(() => tearUp());

  describe('when exist movies panel', () => {

    describe('and there are movies should', () => {

      beforeEach(() => {

        storeSubject$.next(MOVIES);
        fixture.detectChanges();
      });

      it('display a MOVIE_GRID element', () => {
        expect(compiled.querySelector(MOVIE_GRID)).toBeDefined();
      });
    });

    describe('and there are not movies should', () => {

      beforeEach(() => {

        const moviesPanelWithoutMovieList = undefined

        storeSubject$.next(moviesPanelWithoutMovieList);
        fixture.detectChanges();
      });

      it('not display a MOVIE_GRID element', () => {
        expect(compiled.querySelector(MOVIE_GRID)).toBeNull();
      });
    });
  });

  describe('on init should', () => {

    beforeEach(() => {

      component.ngOnInit();
    });

    it('init detail list dispatching a new action', () => {

      expect(moviesActionsServiceMock.getMoviesList).toHaveBeenCalled();
    });

    describe('subscribe for moviesPanel that inits', () => {

      let moviesPanel: MoviesPanel;

      beforeEach(() => {

        moviesPanel = new MoviesPanel();
        moviesPanel.isLoading = false;
      });

      it('isLoading property false', () => {

        storeSubject$.next(moviesPanel);

        expect(component.isLoading).toBeFalse();
      });

      it('insuranceListResponse property', () => {

        storeSubject$.next(moviesPanel);

        expect(component.movies).toBe(moviesPanel.movieList);
      });
    });
  });

  describe('on destroy phase', () => {

    beforeEach(() => {

      component.ngOnInit();

      spyOn(component['moviesPanelSubscription'], 'unsubscribe').and.callThrough();

      component.ngOnDestroy();
    });

    it('should call to unsubscribe', () => {

      expect(component['moviesPanelSubscription'].unsubscribe).toHaveBeenCalled();
    });
  });

  function tearUp() {

    createMocks();
    configureTestingModule();
    initPropertiesForTest();

    fixture.detectChanges();
  }

  function createMocks() {

    moviesActionsServiceMock = new MoviesActionsService(null);
    spyOn(moviesActionsServiceMock, 'getMoviesList').and.stub();
    spyOn(moviesActionsServiceMock, 'getActorsList').and.stub();
    spyOn(moviesActionsServiceMock, 'getCompaniesList').and.stub();

    sidenavActionsServiceMock = new SidenavActionsService(null);
    spyOn(sidenavActionsServiceMock, 'setTitle').and.stub();

    storeSubject$ = new ReplaySubject();

    storeMock = new Store<MoviesPanel>(null, null, null);
    spyOn(storeMock, 'pipe').and.callFake(() => storeSubject$);

    appRoutingNavigationServiceMock = {
      movies: () => {}
    } as AppRoutingNavigationService;
    spyOn(appRoutingNavigationServiceMock, 'movies').and.stub();
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      declarations: [
        MoviesComponent
      ],
      providers: [
        { provide: MoviesActionsService, useValue: moviesActionsServiceMock },
        { provide: SidenavActionsService, useValue: sidenavActionsServiceMock },
        { provide: AppRoutingNavigationService, useValue: appRoutingNavigationServiceMock },
        { provide: Store, useValue: storeMock }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }

  function initPropertiesForTest() {

    fixture = TestBed.createComponent(MoviesComponent);
    component = fixture.componentInstance;
    component.movies = [new Movie()];

    compiled = fixture.nativeElement as HTMLElement;
  }
});
