import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { waitForAsync, inject, TestBed, tick, fakeAsync } from '@angular/core/testing';
import { MoviesListService } from './movies-list.service';


describe('MoviesListService', () => {

  let service: MoviesListService;

  beforeEach(() => tearUp());

  it('should call list service',
  waitForAsync(inject([HttpTestingController], (backend: HttpTestingController) => {
      service.getMoviesList().subscribe();
      backend.expectOne(service['baseUrl']).flush({}, { status: 200, statusText: 'Ok' });
      backend.verify();
    }))
  );

  function tearUp() {
    configureTestingModule();
    initPropertiesForTest();
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        MoviesListService
      ]
    });
  }

  function initPropertiesForTest() {
    service = TestBed.inject(MoviesListService);
  }
});
