import { Movie } from './../../../../model/movie/movie';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MoviesListService {

  constructor(
    private http: HttpClient
  ) {
  }

  getMoviesList(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.baseUrl);
  }

  private get baseUrl() {
    return 'http://localhost:3000' + '/movies';
  }
}
