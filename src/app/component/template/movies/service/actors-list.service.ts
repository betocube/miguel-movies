import { Actor } from './../../../../model/actor/actor';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ActorsListService {

  constructor(
    private http: HttpClient
  ) {
  }

  getActorsList(): Observable<Actor[]> {
    return this.http.get<Actor[]>(this.baseUrl);
  }

  private get baseUrl() {
    return 'http://localhost:3000' + '/actors';
  }
}
