import { Company } from './../../../../model/company/company';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CompaniesListService {

  constructor(
    private http: HttpClient
  ) {
  }

  getCompaniesList(): Observable<Company[]> {
    return this.http.get<Company[]>(this.baseUrl);
  }

  private get baseUrl() {
    return 'http://localhost:3000' + '/companies';
  }
}
