import { ActorsListService } from './../../service/actors-list.service';
import { switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { MoviesGetListAction, MoviesPanelActionTypes, ActorsListGetSuccessAction, ActorsGetListAction } from './../action/movies-panel.action';

@Injectable()
export class GetActorsListEffect {

  constructor(
    private actions$: Actions<MoviesGetListAction>,
    private actorsListService: ActorsListService) { }

  actorsList$ = createEffect(() => this.actions$
    .pipe(
      ofType<ActorsGetListAction>(MoviesPanelActionTypes.GET_ACTORS_LIST),
      switchMap((action) =>  this.actorsListService.getActorsList().pipe(
          switchMap(actorsList => of(new ActorsListGetSuccessAction(actorsList)))
      ))
    ));
}
