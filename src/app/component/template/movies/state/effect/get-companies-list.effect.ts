import { CompaniesListService } from './../../service/companies-list.service';
import { switchMap } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { MoviesPanelActionTypes, CompaniesListGetSuccessAction, CompaniesGetListAction } from './../action/movies-panel.action';

@Injectable()
export class GetCompaniesListEffect {

  constructor(
    private actions$: Actions<CompaniesGetListAction>,
    private companiesListService: CompaniesListService) { }

  companiesList$ = createEffect(() => this.actions$
    .pipe(
      ofType<CompaniesGetListAction>(MoviesPanelActionTypes.GET_COMPANIES_LIST),
      switchMap((action) =>  this.companiesListService.getCompaniesList().pipe(
          switchMap(companiesList => of(new CompaniesListGetSuccessAction(companiesList)))
      ))
    ));
}
