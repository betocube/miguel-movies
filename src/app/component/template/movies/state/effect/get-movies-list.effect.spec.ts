import { MoviesListService } from './../../service/movies-list.service';
import { MoviesGetListAction, MoviesListGetSuccessAction } from './../action/movies-panel.action';
import { Movie } from './../../../../../model/movie/movie';
import { GetMoviesListEffect } from './get-movies-list.effect';
import { HttpClientModule } from '@angular/common/http';
import { TestBed, waitForAsync } from '@angular/core/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { ActionsSubject, StoreModule, Action } from '@ngrx/store';
import { cold, hot } from 'jasmine-marbles';
import { Observable, of } from 'rxjs';

describe('GetMoviesListEffect', () => {

  const MOVIES_LIST_RESPONSE = [new Movie()];
  const ACTION = new MoviesGetListAction();

  let actions: Observable<Movie[]>;
  let effect: GetMoviesListEffect;

  let moviesSrvMock: any;

  beforeEach(() => tearUp());

  it('should return an observable with the action to dispatch', waitForAsync(() => {

    const successAction = new MoviesListGetSuccessAction(MOVIES_LIST_RESPONSE);

    const actions = new ActionsSubject();
    const effects = new GetMoviesListEffect(actions, moviesSrvMock);

    const result: Action[] = [];
    effects.moviesPanel$.subscribe((action) => {
        result.push(action);
    });

    actions.next(ACTION);

    expect(result).toEqual([
      successAction
    ]);
  }));

  function tearUp() {

    createMocks();
    configureTestingModule();
    initPropertiesForTest();
  }

  function createMocks() {

    moviesSrvMock = {
      getMoviesList: () => {
              return of(MOVIES_LIST_RESPONSE);
          },
      };
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        StoreModule
      ],
      providers: [
        GetMoviesListEffect,
        { provide: MoviesListService, useValue: moviesSrvMock },
        provideMockActions(() => actions)
      ]
    });
  }

  function initPropertiesForTest() {

    effect = TestBed.inject(GetMoviesListEffect);
  }
});
