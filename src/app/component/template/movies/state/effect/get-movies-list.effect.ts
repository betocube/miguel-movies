import { switchMap, catchError, mergeMap, map } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { EMPTY, of } from 'rxjs';
import { MoviesListService } from './../../service/movies-list.service';
import { MoviesGetListAction, MoviesPanelActionTypes, MoviesListGetSuccessAction } from './../action/movies-panel.action';

@Injectable()
export class GetMoviesListEffect {

  constructor(
    private actions$: Actions,
    private moviesListService: MoviesListService) { }

  moviesPanel$ = createEffect(() => this.actions$
    .pipe(
      ofType<MoviesGetListAction>(MoviesPanelActionTypes.GET_MOVIES_LIST),
      mergeMap((action) =>  this.moviesListService.getMoviesList().pipe(
          map(moviesList => (new MoviesListGetSuccessAction(moviesList))),
          catchError(() => EMPTY))
      )
    ));
}
