import { MoviesGetListAction } from './../action/movies-panel.action';
import { MoviesPanel } from './../../../../../model/movies-panel/movies-panel';
import { MoviesActionsService } from './movies-actions.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';

describe('MoviesActionsService', () => {

  let moviesActionsService: MoviesActionsService;
  let storeMock: Store<MoviesPanel>;

  beforeEach(() => tearUp());

  it('should dispatch the get invoice list action', () => {
    moviesActionsService.getMoviesList();

    expect(storeMock.dispatch).toHaveBeenCalledWith(new MoviesGetListAction());
  });

  function tearUp () {

    createMocks();
    configureTestingModule();
    initPropertiesForTest();
  }

  function createMocks() {

    storeMock = new Store<MoviesPanel>(null, null, null);
    spyOn(storeMock, 'dispatch').and.callFake(() => {});
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        MoviesActionsService,
        { provide: Store, useValue: storeMock }
      ]
    });
  }

  function initPropertiesForTest() {

    moviesActionsService = TestBed.inject(MoviesActionsService);
  }
});
