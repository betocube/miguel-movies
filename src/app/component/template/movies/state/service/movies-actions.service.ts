import { MoviesGetListAction } from '../action/movies-panel.action';
import { ActorsGetListAction } from '../action/movies-panel.action';
import { CompaniesGetListAction } from '../action/movies-panel.action';
import { MoviesPanel } from '../../../../../model/movies-panel/movies-panel';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class MoviesActionsService {

  constructor(private store: Store<MoviesPanel>) { }

  getMoviesList(): void {
    this.store.dispatch(new MoviesGetListAction());
  }

  getActorsList(): void {
    this.store.dispatch(new ActorsGetListAction());
  }

  getCompaniesList(): void {
    this.store.dispatch(new CompaniesGetListAction());
  }
}
