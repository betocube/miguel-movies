import { Movie } from './../../../../../model/movie/movie';
import { MoviesPanelActionTypes, MoviesListGetSuccessAction } from './../action/movies-panel.action';
import { MoviesPanel } from './../../../../../model/movies-panel/movies-panel';
import { moviesPanelReducer } from './movies-panel.reducer';

describe('moviesPanelReducer', () => {

  let currentState: MoviesPanel;

  beforeEach(() => tearUp());

  describe('on ' + MoviesPanelActionTypes.MOVIES_LIST_SUCCESS + ' should', () => {

    it('return the movies list response', () => {

      const moviesListResponse = [new Movie()];

      const getSuccessAction = new MoviesListGetSuccessAction(moviesListResponse);
      const newState = moviesPanelReducer(currentState, getSuccessAction);
      const newMovieList = newState.movieList;

      expect(newMovieList).toEqual(moviesListResponse);
      expect(newState.isLoading).toBeFalse();
    });
  });

  it('on UNEXPECTED action should return the default session values', () => {

    const action: any = {
      type: 'UNEXPECTED'
    };
    const newState = moviesPanelReducer(undefined, action);

    expect(newState).toEqual(new MoviesPanel());
  });

  function tearUp() {

    currentState = new MoviesPanel();
  }
});
