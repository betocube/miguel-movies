import { Actor } from './../../../../../model/actor/actor';
import { Company } from './../../../../../model/company/company';
import { MoviesPanelActionTypes, MoviesPanelActions } from './../action/movies-panel.action';
import { Movie } from './../../../../../model/movie/movie';
import { MoviesPanel } from './../../../../../model/movies-panel/movies-panel';

export function moviesPanelReducer(state = new MoviesPanel(), action: MoviesPanelActions): MoviesPanel {

  switch (action.type) {

    case MoviesPanelActionTypes.GET_MOVIES_LIST:
      return getMoviesList(state);

    case MoviesPanelActionTypes.MOVIES_LIST_SUCCESS:
      return updateMovies(state, action.payload);

    case MoviesPanelActionTypes.ACTORS_LIST_SUCCESS:
      return updateActors(state, action.payload);

    case MoviesPanelActionTypes.COMPANIES_LIST_SUCCESS:
      return updateCompanies(state, action.payload);

    default:
      return state;
  }
}

function getMoviesList(state: MoviesPanel): MoviesPanel {

  const newState = Object.assign({}, state);

  newState.movieList = []
  newState.isLoading = true;

  return newState;
}

function updateMovies(state: MoviesPanel, newResults: Movie[]): MoviesPanel {

  const stateCopy = Object.assign({}, state)
  stateCopy.movieList = newResults;
  stateCopy.isLoading = false;

  return stateCopy;
}

function updateActors(state: MoviesPanel, newResults: Actor[]): MoviesPanel {

  const stateCopy = Object.assign({}, state)
  stateCopy.actorsList = newResults;
  stateCopy.isLoading = false;

  return stateCopy;
}

function updateCompanies(state: MoviesPanel, newResults: Company[]): MoviesPanel {

  const stateCopy = Object.assign({}, state)
  stateCopy.companiesList = newResults;
  stateCopy.isLoading = false;

  return stateCopy;
}
