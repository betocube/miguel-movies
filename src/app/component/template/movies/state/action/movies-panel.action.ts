import { Company } from './../../../../../model/company/company';
import { Actor } from './../../../../../model/actor/actor';
import { Movie } from './../../../../../model/movie/movie';
import { Action } from '@ngrx/store';

export enum MoviesPanelActionTypes {
  GET_MOVIES_LIST = '[MOVIES_LIST] Get all movies list',
  MOVIES_LIST_SUCCESS = '[MOVIES_LIST] Success',
  GET_ACTORS_LIST = '[ACTORS_LIST] Get all movies list',
  ACTORS_LIST_SUCCESS = '[ACTORS_LIST] Success',
  GET_COMPANIES_LIST = '[COMPANIES_LIST] Get all movies list',
  COMPANIES_LIST_SUCCESS = '[COMPANIES_LIST] Success',
}

export class MoviesGetListAction implements Action {

  readonly type = MoviesPanelActionTypes.GET_MOVIES_LIST;

  constructor() {}
}

export class MoviesListGetSuccessAction implements Action {

  readonly type = MoviesPanelActionTypes.MOVIES_LIST_SUCCESS;

  constructor(public payload: Movie[]) {}
}

export class ActorsGetListAction implements Action {

  readonly type = MoviesPanelActionTypes.GET_ACTORS_LIST;

  constructor() {}
}

export class ActorsListGetSuccessAction implements Action {

  readonly type = MoviesPanelActionTypes.ACTORS_LIST_SUCCESS;

  constructor(public payload: Actor[]) {}
}

export class CompaniesGetListAction implements Action {

  readonly type = MoviesPanelActionTypes.GET_COMPANIES_LIST;

  constructor() {}
}

export class CompaniesListGetSuccessAction implements Action {

  readonly type = MoviesPanelActionTypes.COMPANIES_LIST_SUCCESS;

  constructor(public payload: Company[]) {}
}

export type MoviesPanelActions = MoviesGetListAction |
MoviesListGetSuccessAction |
ActorsGetListAction |
ActorsListGetSuccessAction |
CompaniesGetListAction |
CompaniesListGetSuccessAction;
