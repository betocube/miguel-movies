import { MoviesPanel } from './../../../../../model/movies-panel/movies-panel';
import { createFeatureSelector } from '@ngrx/store';

export const selectStoreMoviesPanel = createFeatureSelector<MoviesPanel>('moviesPanel');

export const selectStoreActorsList = createFeatureSelector<MoviesPanel>('actorsList');

export const selectStoreCompaniesList = createFeatureSelector<MoviesPanel>('companiesList');
