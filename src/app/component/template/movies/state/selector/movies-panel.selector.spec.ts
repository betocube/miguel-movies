import { selectStoreMoviesPanel } from './movies-panel.selector';
import { MoviesPanel } from './../../../../../model/movies-panel/movies-panel';
import { TestBed } from '@angular/core/testing';
import { select, Store, StoreModule } from '@ngrx/store';
import { moviesPanelReducer } from '../reducer/movies-panel.reducer';

describe('Movies panel selector', () => {

  let store: Store<MoviesPanel>;

  beforeEach(() => tearUp());

  it('defines selectStoreMoviesPanel selector', () => {

    store.pipe(select(selectStoreMoviesPanel)).subscribe(moviesPanel => {

      expect(moviesPanel).toEqual(new MoviesPanel());
    });
  });

  function tearUp() {

    configureTestingModule();
    initPropertiesForTest();
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({
          moviesPanel: moviesPanelReducer
        }, {
          runtimeChecks: {
            strictStateImmutability: false,
            strictActionImmutability: true
          }
        })
      ]
    });
  }

  function initPropertiesForTest() {

    store = TestBed.inject(Store);
  }
});
