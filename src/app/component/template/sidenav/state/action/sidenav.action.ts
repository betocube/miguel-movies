import { Movie } from './../../../../../model/movie/movie';
import { Action } from '@ngrx/store';

export enum SidenavActionTypes {
  SET_TITLE = '[SIDENAV] Set title',
}

export class SetTitleAction implements Action {

  readonly type = SidenavActionTypes.SET_TITLE;

  constructor(public payload: string) {}
}

export type SidenavActions = SetTitleAction;
