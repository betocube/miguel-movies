import { select, Store } from '@ngrx/store';
import { switchMap, withLatestFrom } from 'rxjs/operators';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { MovieDetailsGetAction, MovieDetailsGetActionTypes, MovieDetailsGetSuccessAction } from '../action/movie-details.action';
import { MovieDetailsService } from '../../service/movie-details.service';
import { Movie } from '../../../../../model/movie/movie';

@Injectable()
export class GetMovieDetailsEffect {

  constructor(
    private actions$: Actions<MovieDetailsGetAction>,
    private movieDetailsService: MovieDetailsService) { }

    movieDetailsl$ = createEffect(() => this.actions$
    .pipe(
      ofType<MovieDetailsGetAction>(MovieDetailsGetActionTypes.GET_MOVIE_DETAILS),
      switchMap((action: MovieDetailsGetAction) =>  this.movieDetailsService.getMovieDetails(action.payload).pipe(
          switchMap((movie: Movie) => of(new MovieDetailsGetSuccessAction(movie)))
      ))
    ));
}
