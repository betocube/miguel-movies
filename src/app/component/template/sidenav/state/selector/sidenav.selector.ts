import { Sidenav } from './../../../../../model/sidenav/sidenav';
import { createFeatureSelector } from '@ngrx/store';

export const selectStoreSidenav = createFeatureSelector<Sidenav>('sidenav');
