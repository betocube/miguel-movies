import { SidenavActions, SidenavActionTypes } from './../action/sidenav.action';
import { Sidenav } from './../../../../../model/sidenav/sidenav';

const defaultState = new Sidenav();

export function sidenavReducer(state: Sidenav = defaultState, action: SidenavActions): Sidenav {

  switch (action.type) {
    case SidenavActionTypes.SET_TITLE:
      return  updateTitle(state, action.payload);

    default:
      return state;
  }
}

function updateTitle(state: Sidenav, title: string): Sidenav {
  return { ...state, title };
}
