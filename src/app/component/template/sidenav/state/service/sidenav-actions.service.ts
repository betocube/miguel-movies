import { Sidenav } from './../../../../../model/sidenav/sidenav';
import { SetTitleAction } from './../action/sidenav.action';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

@Injectable({
  providedIn: 'root'
})
export class SidenavActionsService {

  constructor(private store: Store<Sidenav>) { }

  setTitle(title: string): void {
    this.store.dispatch(new SetTitleAction(title));
  }
}
