import { selectStoreSidenav } from './../state/selector/sidenav.selector';
import { Store, select } from '@ngrx/store';
import { Sidenav } from './../../../../model/sidenav/sidenav';
import { Observable, Subscription } from 'rxjs';
import { SidenavConfigService } from './../service/sidenav-config.service';
import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { Component, OnInit, ChangeDetectorRef, OnDestroy, AfterViewInit, AfterViewChecked } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';

@Component({
  selector: 'mig-sidenav',
  templateUrl: 'sidenav.component.html',
  styleUrls: ['sidenav.component.scss'],
})
export class SidenavComponent implements AfterViewChecked, OnInit, OnDestroy {

  pageTitle: string = 'mig.movies.title';
  actorsButtonConfig: any;
  companiesButtonConfig: any;
  moviesButtonConfig: any;
  mobileQuery: MediaQueryList;

  private _mobileQueryListener: () => void;
  private sidenav$: Observable<Sidenav>;
  private sidenavSubscription: Subscription;

  constructor(
    private appRoutingNavigationService: AppRoutingNavigationService,
    private configService: SidenavConfigService,
    private changeDetectorRef: ChangeDetectorRef,
    private media: MediaMatcher,
    private store: Store<Sidenav>) {

      this.mobileQuery = this.media.matchMedia('(max-width: 600px)');
      this._mobileQueryListener = () => this.changeDetectorRef.detectChanges();
      this.mobileQuery.addListener(this._mobileQueryListener);
    }

  ngOnInit() {

    this.initConfigs();
  }

  ngAfterViewChecked() {
    this.initSubscription();
    this.changeDetectorRef.detectChanges();
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeListener(this._mobileQueryListener);
  }

  goToMovies() {
    this.pageTitle = 'mig.movies.title';
    this.appRoutingNavigationService.movies();
  }

  private initConfigs() {

    this.companiesButtonConfig = this.configService.buildCompaniesButtonConfig();
    this.actorsButtonConfig = this.configService.buildActorsButtonConfig();
    this.moviesButtonConfig = this.configService.buildMoviesButtonConfig();
  }

  private initSubscription() {

    this.sidenav$ = this.store.pipe(select(selectStoreSidenav));
    this.sidenavSubscription = this.sidenav$.subscribe(sidenav => {

      this.pageTitle = sidenav.title;
    });
  }
}