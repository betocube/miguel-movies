import { ReplaySubject } from 'rxjs';
import { Sidenav } from './../../../../model/sidenav/sidenav';
import { Store } from '@ngrx/store';
import { SidenavConfigService } from './../service/sidenav-config.service';
import { AppRoutingNavigationService } from './../../../../model/route/app-routing-navigation.service';
import { SidenavComponent } from './sidenav.component';
import { NO_ERRORS_SCHEMA, ChangeDetectorRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TranslateModule } from '@ngx-translate/core';

describe('SidenavComponent', () => {

  const SIDENAV_CONTENT = 'mat-sidenav-content';
  const TOOLBAR = 'mat-toolbar';

  const ACTORS_CONFIG = {
    id: 'idActor',
    label: 'labelActor',
    name: 'nameActor'
  }

  const COMPANIES_CONFIG = {
    id: 'idCompany',
    label: 'labelCompany',
    name: 'nameCompany'
  }

  const MOVIES_CONFIG = {
    id: 'idMovie',
    label: 'labelMovie',
    name: 'nameMovie'
  }

  let component: SidenavComponent;
  let fixture: ComponentFixture<SidenavComponent>;
  let configService: SidenavConfigService;
  let compiled: any;

  let appRoutingNavigationServiceMock: any;

  let changeDetectorRefMock: any;
  let storeMock: Store<Sidenav>;

  let storeSubject$: ReplaySubject<Sidenav>;

  beforeEach(() => tearUp());

  it('create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should have a toolbar with the correct title', () => {
    expect(compiled.querySelector(TOOLBAR)?.textContent).toContain('mig.movies.title');
  });

  describe('onInit should ', () => {

    beforeEach(() => {
      component.ngOnInit();
    });

    it('init buttons config', () => {

      expect(component.moviesButtonConfig).toEqual(MOVIES_CONFIG);
      expect(component.companiesButtonConfig).toEqual(COMPANIES_CONFIG);
      expect(component.actorsButtonConfig).toEqual(ACTORS_CONFIG);
    });
  });

  it('on movies click should forward to movies page', () => {

    component.goToMovies();

    expect(appRoutingNavigationServiceMock.movies).toHaveBeenCalled();
  });

  function tearUp() {

    createMocks();
    configureTestingModule();
    initPropertiesForTest();

    fixture.detectChanges();
  }

  function createMocks() {

    storeSubject$ = new ReplaySubject();

    storeMock = new Store<Sidenav>(null, null, null);
    spyOn(storeMock, 'pipe').and.callFake(() => storeSubject$);

    appRoutingNavigationServiceMock = {
      movies: () => {}
    } as AppRoutingNavigationService;
    spyOn(appRoutingNavigationServiceMock, 'movies').and.stub();

    configService = new SidenavConfigService();
    spyOn(configService, 'buildMoviesButtonConfig').and.returnValue(MOVIES_CONFIG);
    spyOn(configService, 'buildCompaniesButtonConfig').and.returnValue(COMPANIES_CONFIG);
    spyOn(configService, 'buildActorsButtonConfig').and.returnValue(ACTORS_CONFIG);

  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        TranslateModule.forRoot()
      ],
      declarations: [
        SidenavComponent
      ],
      providers: [
        { provide: ChangeDetectorRef, useValue: changeDetectorRefMock },
        { provide: AppRoutingNavigationService, useValue: appRoutingNavigationServiceMock },
        { provide: SidenavConfigService, useValue: configService },
        { provide: Store, useValue: storeMock }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    })
    .compileComponents();
  }

  function initPropertiesForTest() {

    fixture = TestBed.createComponent(SidenavComponent);
    component = fixture.componentInstance;
    compiled = fixture.nativeElement as HTMLElement;
  }
});
