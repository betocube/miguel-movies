import { RoutePath } from './../../../model/route/route.path';
import { SidenavComponent } from './component/sidenav.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    component: SidenavComponent,
    children: [
      {
        path: RoutePath.MOVIES,
        loadChildren: () => import('../movies/movies.module').then(m => m.MoviesModule)
      },
      {
        path: `${RoutePath.MOVIE_DETAIL}/:id`,
        loadChildren: () =>
          import('../../organism/movie-details/movie-details.module').then(m => m.MovieDetailsModule)
      },
      {
        path: `${RoutePath.CREATE_NEW_MOVIE}`,
        loadChildren: () =>
          import('../../organism/movie-request-form/movie-request-form.module').then(m => m.MovieRequestModule)
      },
      {
        path: `${RoutePath.EDIT_MOVIE}/:id`,
        loadChildren: () =>
          import('../../organism/movie-request-form/movie-request-form.module').then(m => m.MovieRequestModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SidenavRoutingModule { }