import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { SidenavRoutingModule } from './sidenav-routing.module';
import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { SidenavComponent } from './component/sidenav.component';
import { sidenavReducer } from './state/reducer/sidenav.reducer';

@NgModule({
  declarations: [SidenavComponent],
  imports: [
    TranslateModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
    SidenavRoutingModule,
    StoreModule.forFeature('sidenav', sidenavReducer),
  ],
  providers: [],
})
export class SidenavModule {}