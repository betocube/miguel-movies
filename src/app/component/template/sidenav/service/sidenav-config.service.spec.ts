import { SidenavConfigService } from './sidenav-config.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

describe('InvoiceDocUploadHintConfigService', () => {


  const ACTORS_CONFIG = {
    id: SidenavConfigService.ACTORS_BUTTON_ID,
    label: SidenavConfigService.ACTORS_BUTTON_LABEL,
    name: SidenavConfigService.ACTORS_BUTTON_NAME
  };

  const COMPANIES_CONFIG = {
    id: SidenavConfigService.COMPANIES_BUTTON_ID,
    label: SidenavConfigService.COMPANIES_BUTTON_LABEL,
    name: SidenavConfigService.COMPANIES_BUTTON_NAME
  };

  const MOVIES_CONFIG = {
    id: SidenavConfigService.MOVIES_BUTTON_ID,
    label: SidenavConfigService.MOVIES_BUTTON_LABEL,
    name: SidenavConfigService.MOVIES_BUTTON_NAME
  };

  let service: SidenavConfigService;

  beforeEach(() => tearUp());

  it('buildActorsButtonConfig that returns the actors button config', () => {

    expect(service.buildActorsButtonConfig()).toEqual(ACTORS_CONFIG);
  });

  it('buildCompaniesButtonConfig that returns the companies button config', () => {

    expect(service.buildCompaniesButtonConfig()).toEqual(COMPANIES_CONFIG);
  });

  it('buildMoviesButtonConfig that returns the movies button config', () => {

    expect(service.buildMoviesButtonConfig()).toEqual(MOVIES_CONFIG);
  });

  function tearUp() {

    configureTestingModule();
    initPropertiesForTest();
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        SidenavConfigService
      ]
    });
  }

  function initPropertiesForTest() {

    service = TestBed.inject(SidenavConfigService);
  }
});
