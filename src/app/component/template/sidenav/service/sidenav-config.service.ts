import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidenavConfigService {

  static MOVIES_BUTTON_LABEL = 'mig.sidenav.movies';
  static MOVIES_BUTTON_ID = SidenavConfigService.MOVIES_BUTTON_LABEL;
  static MOVIES_BUTTON_NAME = SidenavConfigService.MOVIES_BUTTON_LABEL;

  static COMPANIES_BUTTON_LABEL = 'mig.sidenav.companies';
  static COMPANIES_BUTTON_ID = SidenavConfigService.COMPANIES_BUTTON_LABEL;
  static COMPANIES_BUTTON_NAME = SidenavConfigService.COMPANIES_BUTTON_LABEL;

  static ACTORS_BUTTON_LABEL = 'mig.sidenav.actors';
  static ACTORS_BUTTON_ID = SidenavConfigService.ACTORS_BUTTON_LABEL;
  static ACTORS_BUTTON_NAME = SidenavConfigService.ACTORS_BUTTON_LABEL;

  buildMoviesButtonConfig(): any {

    return {
      id: SidenavConfigService.MOVIES_BUTTON_ID,
      label: SidenavConfigService.MOVIES_BUTTON_LABEL,
      name: SidenavConfigService.MOVIES_BUTTON_NAME
    };
  }

  buildActorsButtonConfig(): any {

    return {
      id: SidenavConfigService.ACTORS_BUTTON_ID,
      label: SidenavConfigService.ACTORS_BUTTON_LABEL,
      name: SidenavConfigService.ACTORS_BUTTON_NAME
    };
  }

  buildCompaniesButtonConfig(): any {

    return {
      id: SidenavConfigService.COMPANIES_BUTTON_ID,
      label: SidenavConfigService.COMPANIES_BUTTON_LABEL,
      name: SidenavConfigService.COMPANIES_BUTTON_NAME
    };
  }
}
