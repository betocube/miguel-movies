import { TranslateModule } from '@ngx-translate/core';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';

import { NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

describe('AppComponent', () => {

  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(() => tearUp());

  describe('should ', () => {
    it('create the app', () => {
      expect(component).toBeTruthy();
    });
  });

  function tearUp() {

    configureTestingModule();
    initPropertiesForTest();

    fixture.detectChanges();
  }

  function configureTestingModule() {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        TranslateModule.forRoot()
      ],
      declarations: [
        AppComponent
      ],
      schemas: [ NO_ERRORS_SCHEMA ]
    })
    .compileComponents();
  }

  function initPropertiesForTest() {

    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
  }
});
