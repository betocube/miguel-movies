import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { HttpErrorResponseHandlerService } from '../service/http-error-response-handler.service';

@Injectable()
export class HttpResponseInterceptor implements HttpInterceptor {

  constructor(private httpErrorResponseHandlerService: HttpErrorResponseHandlerService) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return next.handle(request).pipe(tap(
      (event: HttpEvent<any>) => {

      }, (error: any) => {

        this.httpErrorResponseHandlerService.handle(error);
      }
    ));
  }
}
