import { HttpResponseInterceptor } from './interceptor/http-response.interceptor';
import { HttpErrorResponseHandlerService } from './service/http-error-response-handler.service';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ModuleWithProviders, NgModule } from '@angular/core';



@NgModule({
  exports: [
    HttpClientModule
  ],
  imports: [
    HttpClientModule
  ]
})
export class HttpModule {

  static forRoot() {
    return {
      ngModule: HttpModule,
      providers: [
        HttpErrorResponseHandlerService,
        {
          provide: HTTP_INTERCEPTORS,
          useClass: HttpResponseInterceptor,
          multi: true
        }
      ]
    };
  }
}
