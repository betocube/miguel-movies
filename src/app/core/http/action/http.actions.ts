import { Action } from '@ngrx/store';

import { HttpStatusCode } from '../model/http-status-code';

export enum HttpActionsTypes {

  NOTIFY_ERROR_STATUS_CODE = '[HTTP] Notify error status code',
  SAVE_LAST_ACTION = '[HTTP] Save last action',
  REQUEST_TOKEN = '[HTTP] Request token',
  CLEAR_SESSION = '[HTTP] Clear session',
}

export class HttpNotifyErrorStatusCodeAction implements Action {

  readonly type = HttpActionsTypes.NOTIFY_ERROR_STATUS_CODE;

  constructor(public payload: HttpStatusCode) { }
}

export type HttpActions = HttpNotifyErrorStatusCodeAction;
