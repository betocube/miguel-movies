import { AppRoutingNavigationService } from './../../../model/route/app-routing-navigation.service';
import { HttpStatusCodeClass } from './../model/http-status-code';
import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class HttpErrorResponseHandlerService {

  constructor(
    private appRoutingNavigationService: AppRoutingNavigationService
  ) { }

  handle(event: HttpErrorResponse) {

    if (this.shouldRedirectToHomePage(event)) {
      this.appRoutingNavigationService.movies();
      return;
    }
  }

  private shouldRedirectToHomePage(event: HttpErrorResponse): boolean {

    return HttpStatusCodeClass.isNotFound(event.status);
  }
}
